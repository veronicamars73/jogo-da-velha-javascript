img1 = "url('img/1.jpg')";
img2 = "url('img/0.jpg')";

vez = img1;

contador = 0; //contador de Jogadas

lana = 0; //contador de vitória de lana
gaga = 0; //contador de vitória de gaga
empate = 0; //contador dos empates

function trocaVez(){
	if(vez == img1){
		vez = img2;
		document.getElementById("jogador").innerHTML = "Lana";
	}else{
		vez = img1;
		document.getElementById("jogador").innerHTML = "Gaga";
	}
}

//Deixa a tela branca, avá
function limpatela(){
	var casas = document.getElementsByClassName('casa'); //pega todos os elementos que tem classe 'casa'
	for (var i = 0; i < casas.length; i++) { //pega o vetor dos elementos (cada elemnto do vetor é uma casa)
  		casas[i].style.background = ''; //tira a imagem que tá na casa
	}
}
//Atualiza a tabelinha que tem o placar 
function atualizaplacar(){
	document.getElementById("lana").innerHTML = lana; //Coloca o numero de vitórias de lana
	document.getElementById("gaga").innerHTML = gaga;//Coloca o numero de vitórias de gaga
	document.getElementById("empate").innerHTML = empate; //Coloca o numero de empates
}

// Zerar e atualizar o placar 
function novojogo(vez){
	//Dando ponto ao vencedor
	if(vez == img1){
		gaga += 1;
		document.getElementById("status").innerHTML = "O vencedor da partida foi a Lady Gaga"; //Coloca o vencedor no html
	}else{
		if(vez == img2){
			lana += 1;
			document.getElementById("status").innerHTML = "O vencedor da partida foi a Lana del Rey"; //Coloca o vencedor no html
		}else{
			empate += 1;
			document.getElementById("status").innerHTML = "O jogo terminou empatado"; //Coloca o vencedor no html
		}
	}
	//Placar
	atualizaplacar();
	//Zerando as coisas
	vez = img1; // restaarta a vez
	contador = 0; // restaarta o número de jogadas
}

function jogar(c){
	console.log("click em casa " + c.id);
	bg = c.style.backgroundImage;
	if(contador==0){
		document.getElementById("status").innerHTML = "Jogo em andamento"; //muda o status da partida
	}
	if(bg=='' || bg==null){	
		c.style.backgroundImage = vez;
		contador++; //Uma jogada foi realizada
		if(contador >= 5){ //só tem vencedor a partir de cinco jogadinhas
			if(ganhador()){ //Se tiver ganhador
				limpatela();
				novojogo(vez);
			}else{
				if(contador==9){ //se já tiverem jogado as noves e não tiver ganhador é empate
				limpatela();
				novojogo("empate");
				}
			}
		}
		trocaVez();
	}
}

function comparaCasa(a,b,c){	
	c1 = document.getElementById('c'+a);
	c2 = document.getElementById('c'+b);
	c3 = document.getElementById('c'+c);
	b1 = c1.style.backgroundImage;
	b2 = c2.style.backgroundImage;
	b3 = c3.style.backgroundImage;
	if (b1==b2 && b2==b3 && b1!='none' && b1!=''){
		return true;
	}
	
	return false;
}

function ganhador(){
	if(
		comparaCasa(1,2,3) ||
		comparaCasa(4,5,6) ||
		comparaCasa(7,8,9) ||
		comparaCasa(1,4,7) ||
		comparaCasa(2,5,8) ||
		comparaCasa(3,6,9) ||
		comparaCasa(1,5,9) ||
		comparaCasa(3,5,7)
	){
		return true; //true significa que teve vencedor
	}else{
		return false; //false que não teve vencedor
	}
}

